{ config, lib, pkgs, ... }:

let
  defaultUserName = "iwan";
  defaultPassword = "0000";
in
{
  config = {
    users.users."${defaultUserName}" = {
      isNormalUser = true;
      password = defaultPassword;
      extraGroups = [
        "dialout"
        "feedbackd"
        "networkmanager"
        "video"
        "audio"
        "pipewire"
        "wheel"
      ];
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCa9Z7vp2mWiZSOp3f3EzfquDWkULVwLw22WTKUqaGh7oHj9Q99jxgmA7dNDnVUCCy/PXQJ+U13/Q+ZqPSznPRznzAiTfSdH5jyNApSRqgYvLIg+Ceim4soAf1dm6wJkJlEPAPlTjDmiQBx3CYA0lMJHUkK6WP3gOzZCQzv2h+iKvcZw1cCrYppAbm/VxoSrHWfVKzQYyce0v6n2kvN44BnUAY5LMFdpefNqwoLSw+/k+JZy3pS37bovHEPEFqAQNsEd4zTZ2O2ebqz2960D/IjnBnJ4V11aBpk+KBD/Czorq/wcUirDUte4l+JifZS1sgN+7fQf9GHj7iBXghrjbj5 iwan@ngin"
      ];
    };

    # systemd.services.phosh.environment = {
    #   WLR_RENDERER_ALLOW_SOFTWARE = "1";
    # };

    nix = {
      package = pkgs.nixUnstable; # or versioned attributes like nix_2_4
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
     };

    networking.hostName = "pwan";

    time.timeZone = "Europe/Brussels";

    i18n.defaultLocale = "en_GB.UTF-8";
    console = {
      font = "Lat2-Terminus16";
      keyMap = "be-latin1";
    };

    services.xserver.desktopManager.phosh = {
      enable = true;
      user = defaultUserName;
      group = "users";
    };

    hardware.bluetooth.enable = true;

    sound.enable = true;

    hardware.pulseaudio.enable = false;

    security.rtkit.enable = true;

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    #   jack.enable = false;
      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };

    services.gnome.evolution-data-server.enable = true;

    programs.calls.enable = true;
    hardware.sensor.iio.enable = false;

    environment.systemPackages = [
      pkgs.chatty
      pkgs.kgx
      pkgs.megapixels
      pkgs.epiphany
      pkgs.firefox
      pkgs.git
      pkgs.gnome.gnome-calculator
      pkgs.gnome-2048
      pkgs.evince
      pkgs.gnome.gedit
      pkgs.gnome.gnome-maps
      pkgs.gnome.gnome-weather
      pkgs.gnome.gnome-sound-recorder
      pkgs.gnome.eog
      pkgs.gnome.gnome-contacts
      pkgs.gnome-feeds
      pkgs.gnome-usage
      pkgs.gnome.gnome-clocks
      pkgs.gnome.nautilus
      pkgs.gnome.geary
      pkgs.gnome.gnome-notes
      pkgs.gnome.totem
      pkgs.gnome.gnome-calendar
      pkgs.gnome-photos
      pkgs.gnome.gnome-music
      # pkgs.dialect
      pkgs.shortwave
      pkgs.numberstation
      # Not in cache, heavy build
      pkgs.fractal
      pkgs.spot
      pkgs.newsflash
      pkgs.pavucontrol
    ];

    services.openssh = {
      enable = true;
      passwordAuthentication = false;
    };

    system.stateVersion = "22.05";

  };
}
