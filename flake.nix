{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.mobile-nixos = {
    url = "github:NixOS/mobile-nixos";
    flake = false;
  };

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, mobile-nixos, flake-utils }:
  {
    nixosConfigurations.pwan = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules =
        [
          ({ pkgs, ... }: {
            # Pinning nixpkgs to the system one by default
            nix.registry.nixpkgs.flake = nixpkgs;
          })
          (import ./configuration.nix)
          (import "${mobile-nixos}/lib/configuration.nix" {
            device = "pine64-pinephone";
          })
        ];
    };
    nixosConfigurations.x86 = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules =
        [
          ({ pkgs, ... }: {
            # Pinning nixpkgs to the system one by default
            nix.registry.nixpkgs.flake = nixpkgs;
          })
          (import ./configuration.nix)
          (import "${mobile-nixos}/lib/configuration.nix" {
            device = "uefi-x86_64";
          })
        ];
    };
    x86-vm =
      (import "${mobile-nixos}/lib/eval-with-configuration.nix" {
        configuration = [ (import ./configuration.nix) ];
        device = "uefi-x86_64";
        pkgs = nixpkgs.legacyPackages."x86_64-linux";
      }).outputs.vm;
    pinephone-disk-image =
      (import "${mobile-nixos}/lib/eval-with-configuration.nix" {
        configuration = [ (import ./configuration.nix) ];
        device = "pine64-pinephone";
        pkgs = nixpkgs.legacyPackages."aarch64-linux";
      }).outputs.disk-image;
 } // (flake-utils.lib.eachDefaultSystem (system:
    {
      packages.spot = nixpkgs.legacyPackages.${system}.spot;
    }
  ));
}
